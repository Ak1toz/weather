const BOGOTACOORD = {
    lat: '4.6097',
    lon: '-74.0817'
} 
$(document).ready(function () {
    function windDirection(degrees) {
        const directions = ['north', 'northeast', 'east', 'southeast', 'south', 'southwest', 'west', 'northwest'];
        degrees = degrees * 8 / 360;
        degrees = Math.round(degrees, 0);
        degrees = (degrees + 8) % 8
        return directions[degrees];
    }

    function getWeather(el, data) {
        let t = data.main.temp.toFixed(0);
        $('#locations-temp-' + el).prepend(t);
        $('#locations-humidity-' + el).append('Humidity ' + data.main.humidity + '%');
        $('#locations-speed-' + el).append(3.6 * data.wind.speed.toFixed(0) + ' km/h');
        let wind = windDirection(data.wind.deg);
        $('#locations-wind-' + el).append(wind);
        document.getElementById('locations-icon-' + el).src = `http://openweathermap.org/img/wn/${data.weather[0].icon}.png`;  
    }

    function getWeatherData(name) {
        $.ajax({
            url: `https://api.openweathermap.org/data/2.5/weather?q=${name}&units=metric&appid=c92a4299a9f95615ddc7cd8d991432ce`,
            success: function (data) {

                getWeather(name, data);
                
            },
            error: function (xhr) {
                alert('error');
            }
        });
    }

    function getWeatherDataComplete(coord, callback) {
        $.ajax({
            url: `https://api.openweathermap.org/data/2.5/onecall?lat=${coord.lat}&lon=${coord.lon}&exclude=minutely,hourly,alerts&units=metric&appid=c92a4299a9f95615ddc7cd8d991432ce`,
            success: function (data) {
                callback(data);                
            },
            error: function (xhr) {
                alert('error');
            }
        });
    }

    function callBackMoreInfo(data) {
        console.log(data);
        
        let temp, icon;
            let d = new Date();
            let weekday = new Array(7);
            weekday[0] = "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";

            let dayName = d.getDay();
            dayName++;
            let t = data.current.temp.toFixed(0);

            function dayNames(el, day) {
                switch (day) {
                    case 7:
                        day = 0
                        break;
                    case 8:
                        day = 1
                        break;
                    case 9:
                        day = 2
                        break;
                };
                $('.days-title-' + el).prepend(weekday[day]);
            }
            function dailyIcon(icon) {
                icon[0].src = `http://openweathermap.org/img/wn/${data.daily[0].weather[0].icon}.png`;
                icon[1].src = `http://openweathermap.org/img/wn/${data.daily[0].weather[0].icon}.png`;
            }

            $('#current-state').append(data.current.weather[0].main);
            $('#current-temp').prepend(t);
            document.getElementById('current-icon-state').src = `http://openweathermap.org/img/wn/${data.current.weather[0].icon}.png`;

            dayNames('first', dayName++);
            dayNames('second', dayName++);
            dayNames('last', dayName++);

            $('.days-state-first').append(data.daily[0].weather[0].main);
            $('.days-state-second').append(data.daily[1].weather[0].main);
            $('.days-state-last').append(data.daily[2].weather[0].main);

            icon = document.getElementsByClassName('days-icon-first');
            dailyIcon(icon);
            icon = document.getElementsByClassName('days-icon-second');
            dailyIcon(icon);
            icon = document.getElementsByClassName('days-icon-last');
            dailyIcon(icon);

            for (let index = 0; index < 3; index++) {
                temp = `${data.daily[index].temp.min.toFixed(0)}<span>°<sup>c</sup></span> / ${data.daily[index].temp.max.toFixed(0)}<span>°<sup>c</sup></span>`;
                $('.days-temp-' + index).prepend(temp);
            }
    }

    getWeatherDataComplete(BOGOTACOORD, callBackMoreInfo);
    getWeatherData('lyon', null);
    getWeatherData('paris', null);
});